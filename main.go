package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
)

// Data structure
type Data struct {
	Email     string `json:"email"`
	CreatedAt string `json:"created_at"`
}

// User structure
type User struct {
	Nick string `json:"Nick"`
	Data
	Subs []Data `json:"Subscribers"`
}

// Output to .json structure
type Output struct {
	ID     int    `json:"id"`
	Source string `json:"from"`
	Dest   string `json:"to"`
	Path   []Data `json:"path,omitempty"`
}

// representation of graph
type point struct {
	user   User
	parent int
}

// parse json file to user struct
func fromJSON(filename string) ([]User, error) {
	fileUsers, err := os.Open(filename)

	if err != nil {
		return nil, fmt.Errorf("can't open the file: %s", filename)
	}
	defer fileUsers.Close()

	var decoder *json.Decoder = json.NewDecoder(fileUsers)

	var users []User

	err = decoder.Decode(&users)

	if err != nil {
		return nil, fmt.Errorf("can't decode the file: %s", filename)
	}

	return users, nil
}

// parse user struct to json format
func toJSON(filename string, o []Output) error {
	fileResults, err := os.Create(filename)

	if err != nil {
		return fmt.Errorf("can't create the file: %s", filename)
	}
	defer fileResults.Close()

	var encoder *json.Encoder = json.NewEncoder(fileResults)

	encoder.SetIndent("", "\t")

	err = encoder.Encode(o)

	if err != nil {
		return fmt.Errorf("can't encode data of struct")
	}

	return nil
}

// fill the slice by the output data
func fillSlice(users []User, getIndex map[string]int) ([]Output, error) {
	inputfile := "input.csv"
	fileInput, err := os.Open(inputfile)

	if err != nil {
		return nil, fmt.Errorf("can't open the file: %s", inputfile)
	}

	defer fileInput.Close()

	reader := bufio.NewReader(fileInput)

	var index int

	var o []Output

	for {
		var text string
		text, err = reader.ReadString('\n')

		if err == io.EOF {
			break
		}

		text = strings.Trim(text, "\n")
		arr := strings.Split(text, ",")

		tempID := index + 1 // nolint:gomnd // line numbers should start from 1
		tempSource := arr[0]
		tempDest := arr[1]

		var tempPath []Data

		temp := Output{tempID, tempSource, tempDest, tempPath}

		temp.Path, err = temp.findPath(users, getIndex)

		if err != nil {
			return nil, err
		}

		o = append(o, temp)

		index++
	}

	return o, nil
}

func setIndices(getIndex map[string]int, o Output) (int, int, error) {
	sourceindex, ok := getIndex[o.Source]

	if !ok {
		return -1, -1, fmt.Errorf("user from was not found")
	}

	destindex, ok := getIndex[o.Dest]

	if !ok {
		return -1, -1, fmt.Errorf("user to was not found")
	}

	return sourceindex, destindex, nil
}

// find the path
func (o Output) findPath(users []User, getIndex map[string]int) ([]Data, error) {
	passed := make(map[string]bool)

	var points []point

	sourceindex, destindex, err := setIndices(getIndex, o)

	if err != nil {
		return nil, err
	}

	find := false
	solutionIndex := -1

	var result []Data

	var cur = point{users[destindex], -1}
	points = append(points, cur)

	for i := 0; i != len(points) && !find; i++ {
		passed[points[i].user.Email] = true

		if points[i].user.Email != users[sourceindex].Email {
			for _, s := range points[i].user.Subs {
				_, ok := passed[s.Email]
				if ok {
					continue
				}

				tempindex := getIndex[s.Email]
				temp := point{users[tempindex], i}
				points = append(points, temp)
				passed[temp.user.Email] = true

				if tempindex == sourceindex {
					find = true
					solutionIndex = len(points) - 1 // nolint:gomnd // length of slice more than his last index by 1

					break
				}
			}
		}
	}

	if find {
		cur := solutionIndex

		for temp := points[points[cur].parent]; temp.parent != -1; temp = points[points[cur].parent] {
			result = append(result, temp.user.Data)
			cur = points[cur].parent
		}
	}

	return result, nil
}

func main() {
	filename := "users.json"

	users, err := fromJSON(filename)

	if err != nil {
		fmt.Print(err.Error())
		return
	}

	getIndex := make(map[string]int)

	for i, u := range users {
		getIndex[u.Email] = i
	}

	inputfile := "input.csv"
	fileInput, err := os.Open(inputfile)

	if err != nil {
		err = fmt.Errorf("can't open the file: %s", inputfile)
		fmt.Print(err.Error())

		return
	}

	defer fileInput.Close()

	var o []Output

	o, err = fillSlice(users, getIndex)

	if err != nil {
		fmt.Print(err.Error())
		return
	}

	resultFileName := "results.json"

	err = toJSON(resultFileName, o)

	if err != nil {
		fmt.Print(err.Error())
		return
	}
}
